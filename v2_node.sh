#!/usr/bin/env bash
echo=echo
for cmd in echo /bin/echo; do
    $cmd > /dev/null 2>&1 || continue

    if ! $cmd -e "" | grep -qE '^-e'; then
        echo=$cmd
        break
    fi
done

CSI=$($echo -e "\033[")
CEND="${CSI}0m"
CDGREEN="${CSI}32m"
CRED="${CSI}1;31m"
CGREEN="${CSI}1;32m"
CYELLOW="${CSI}1;33m"
CBLUE="${CSI}1;34m"
CMAGENTA="${CSI}1;35m"
CCYAN="${CSI}1;36m"
CSUCCESS="$CDGREEN"
CFAILURE="$CRED"
CQUESTION="$CMAGENTA"
CWARNING="$CYELLOW"
CMSG="$CCYAN"

if [[ -f /etc/redhat-release ]]; then
    release="centos"
elif cat /etc/issue | grep -q -E -i "raspbian|debian"; then
    release="debian"
elif cat /etc/issue | grep -q -E -i "ubuntu"; then
    release="ubuntu"
elif cat /etc/issue | grep -q -E -i "centos|red hat|redhat"; then
    release="centos"
elif cat /proc/version | grep -q -E -i "raspbian|debian"; then
    release="debian"
elif cat /proc/version | grep -q -E -i "ubuntu"; then
    release="ubuntu"
elif cat /proc/version | grep -q -E -i "centos|red hat|redhat"; then
    release="centos"
else
    echo -e "${CRED}[错误] 不支持的操作系统！${CEND}"
    exit 1
fi

echo -e "${CYELLOW}[信息] 正在更新系统中！${CEND}"
if [[ ${release} == "centos" ]]; then
    yum update -y
else
    apt update -y
fi

echo -e "${CYELLOW}[信息] 正在安装依赖中！${CEND}"
if [[ ${release} == "centos" ]]; then
    yum install nano ntpdate git wget -y
	systemctl stop firewalld
	systemctl disable firewalld
else
    apt install nano ntpdate git wget -y
fi

echo -e "${CYELLOW}[信息] 正在同步时间中！${CEND}"
timedatectl set-timezone Asia/Shanghai
ntpdate ntp1.aliyun.com

echo -e "${CYELLOW}[信息] 创建文件夹中！${CEND}"
rm -rf /opt/v2ray
rm -f /etc/systemd/system/v2ray@.service
mkdir /opt/v2ray

echo -e "${CYELLOW}[信息] 正在安装后端中！${CEND}"
git clone https://gitlab.com/aipeach/v2ray.git /opt/v2ray/default
wget -N -O /etc/systemd/system/v2ray@.service https://gitlab.com/aipeach/v2ray/raw/master/v2ray@.service
chmod -R 755 /opt/v2ray

systemctl daemon-reload

echo -e "${CYELLOW}[信息] 安装完毕！${CEND}"
exit 0